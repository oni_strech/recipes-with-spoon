# Architecture

```
                                               +---------+
                                               |         |
                                     +---------+  Nginx  +---------+
                                     |         |         |         |
                                     |         +----^----+         |
                                     |              |              |
                                     |              |              |
                                     |              |              |
                             +-------v-------+      |      +-------v-------+
                             |               |      |      |               |
                             |  API          |      |      |  SPA          |
                             |  Application  |      +------+  Application  |
                             |               |             |               |
                             +-------+-------+             +---------------+
                                     |
                                     |
                                     |
+-------------------+         +------v------+
|                   |         |             |
|  Synchronization  +--------->  Memcached  |
|                   |         |             |
+-------------------+         +-------------+
```

The main idea of this solution is to reduce the contentful load and latency, but stay as much up-to-date
as possible. Also to offload API from the data representation details (Markdown format of the recipe description)
this part was moved to the client-side.

Memcached and Synchronization is not required to be present, but they guarantee performance improvement and cache synchronization.

# Motivation

Since I receive a bit controversal requirements. From one side `we advise you to not invest more than 3-4 hours`,
but from another side `The application should be as production-ready as possible` I decide to highlight the motivation
behind certain decisions.

#### Recipe `/recipes/<id>` cached forever

This is done to avoid additional requests to Contentful and in pair with Synchronization process
they guarantee that recipe stays up-to-data. Technically depending on implementation we can either
cache it with some TTL we consider reasonable or disable it at all.

#### Synchronization doesn't handle deletions

Yes, this is done to avoid spending too much time, but to show the idea. After some analysis I can say that
this endpoint is a quite limited implementation of Kafka over HTTP with limited functionality.

#### No GraphQL library used

Unfortunately, I didn't find a satistying library which can have models auto-generated from schema.
So the decision was done to go PORO with less dependencies and overhead.

#### No tests

To save time, I decide to skip this, but if I have time to do, then I will go with RSpec, Webmock.
The whole API implemented in a modular way and each component can be easily tested with unit-tests.

#### Not a Sinatra/Rails, but Hanami?

Decision to go with a modular hanami was chosen by several reasons. Personally I think hanami is great,
performant framework with solid and modern approaches. Of course it can be replaced with a smaller setup
with Sinatra let's say, but in case of a future development the benefits are on a Hanami side.

#### Isn't it a compex setup?

Maybe, but it's close to production ready as possible. Definitely certain things can be omitted, but overall
it should be fine.

# How to run

1. Copy .env.example and fill-in the values

   ```bash
   $ cp .env.example .env
   ```

   :warning: Please don't forget to fill in secrets

   (it's better not to store any kind-of credentials anywhere)

1. Build all the defined services

   ```bash
   $ docker-compose build
   ```

1. Run all of them (you should see their logs)

   ```bash
   $ docker-compose up
   ```

1. Open http://localhost:5000

# What can be done better?

1. Contentful client can be definitely improved to reuse/share certain components
2. GraphQL schema as a contract for models/structs
3. Frontend part done quite rough, but enough to operate
4. Synchronization process done very straighforward
