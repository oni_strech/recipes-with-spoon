FROM ruby:2.7.1-alpine

RUN apk add --no-cache \
        build-base~=0.5 \
        libcurl~=7.69

WORKDIR /app

ENV BUNDLER_VERSION 2.1.4

COPY Gemfile* ./
RUN    gem install bundler -v "$BUNDLER_VERSION" \
    && bundle config set without "development test" \
    && bundle install --jobs "$(grep -c ^processor /proc/cpuinfo)" \
    && bundle binstubs --path /app/bin --standalone --force puma rake

COPY bin         ./bin
COPY app         ./app
COPY lib         ./lib
COPY config      ./config
COPY Rakefile \
     config.ru \
     ./

ENTRYPOINT ["./bin/puma"]
CMD ["-C", "config/puma.rb", "config.ru"]
