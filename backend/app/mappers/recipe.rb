# frozen_string_literal: true

module Mappers
  module Recipe
    module_function

    def build(payload)
      Models::Recipe.new(
        id: payload.dig('sys', 'id'),
        title: payload['title'],
        description: payload['description'],
        chef_name: payload.dig('chef', 'name'),
        image_url: payload.dig('photo', 'url'),
        tags: Array(payload.dig('tagsCollection', 'items')).map { |tag| tag['name'] }
      )
    end
  end
end
