# frozen_string_literal: true

module Models
  class Recipe < Dry::Struct
    attribute :id, Types::String
    attribute :title, Types::String
    attribute :image_url, Types::String
    attribute :description, Types::String
    attribute :chef_name, Types::String.optional
    attribute :tags, Types::Array.of(Types::String)

    def to_json(_opts = nil)
      MultiJson.dump(to_h)
    end
  end
end
