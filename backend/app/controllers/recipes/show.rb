# frozen_string_literal: true

module Controllers
  module Recipes
    class Show
      include Hanami::Action

      accept :json

      def call(params)
        result = Cache.fetch("recipe:#{params[:id]}") do
          Queries::Recipe.new.call(params[:id])
        end

        self.status = result.success? ? 200 : 502
        self.body = MultiJson.dump(result)
      end
    end
  end
end
