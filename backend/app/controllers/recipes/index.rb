# frozen_string_literal: true

module Controllers
  module Recipes
    class Index
      include Hanami::Action

      ITEMS_PER_PAGE = 10

      accept :json

      def call(params)
        result =
          Queries::Recipes
            .new
            .limit(ITEMS_PER_PAGE)
            .offset(params[:page].to_i * ITEMS_PER_PAGE)
            .call

        self.status = result.success? ? 200 : 502
        self.body = MultiJson.dump(result)
      end
    end
  end
end
