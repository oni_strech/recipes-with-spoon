# frozen_string_literal: true

module Queries
  class Recipe
    QUERY = <<~GRAPHQL
      query($preview: Boolean, $id: String!) {
        recipe(preview: $preview, id: $id) {
          sys {
            id
          }
          title
          description
          chef(preview: $preview) {
            name
          }
          photo(preview: $preview) {
            title
            url
          }
          tagsCollection(preview: $preview) {
            items {
            name
          }
        }
      }
    }
    GRAPHQL

    def call(id)
      query = {query: QUERY, variables: {preview: false, id: id}}

      response = Contentful::Graphql.post(query)
      return Result.failure(error: 'Oops, something went wrong!') if response.failure?

      recipe = build_recipe(MultiJson.load(response.body))
      Result.success(recipe)
    rescue MultiJson::ParseError, StandardError
      Result.failure(error: 'Oops, something went wrong!')
    end

    private

    def build_recipe(payload)
      recipe = payload.dig('data', 'recipe')
      raise StandardError, 'Recipe not found' unless recipe

      Mappers::Recipe.build(recipe)
    end
  end
end
