# frozen_string_literal: true

module Queries
  class Recipes
    DEFAULT_LIMIT = 100
    DEFAULT_OFFSET = 0
    QUERY = <<~GRAPHQL
      query ($preview: Boolean, $limit: Int, $offset: Int) {
        recipeCollection(preview: $preview, limit: $limit, skip: $offset) {
          items {
            sys {
              id
            }
            title
            description
            chef(preview: $preview) {
              name
            }
            photo(preview: $preview) {
              url
            }
            tagsCollection(preview: $preview) {
              items {
              name
            }
          }
        }
      }
    }
    GRAPHQL

    def limit(value)
      @limit = value
      self
    end

    def offset(value)
      @offset = value
      self
    end

    def call
      query = {
        query: QUERY,
        variables: {
          preview: false,
          limit: @limit || DEFAULT_LIMIT,
          offset: @offset || DEFAULT_OFFSET
        }
      }

      response = Contentful::Graphql.post(query)
      return Result.failure(error: 'Oops, something went wrong!') if response.failure?

      recipes = build_recipes(MultiJson.load(response.body))
      Result.success(recipes)
    rescue MultiJson::ParseError, StandardError
      Result.failure(error: 'Oops, something went wrong!')
    end

    private

    def build_recipes(payload)
      Array(payload.dig('data', 'recipeCollection', 'items')).map do |recipe|
        Mappers::Recipe.build(recipe)
      end
    end
  end
end
