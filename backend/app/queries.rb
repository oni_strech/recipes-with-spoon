# frozen_string_literal: true

module Queries
  class Result
    def self.success(data)
      new(data, success: true)
    end

    def self.failure(data)
      new(data, success: false)
    end

    def initialize(data, success: true)
      @data = data
      @success = success
    end

    def success?
      @success
    end

    def failure?
      !success?
    end

    def to_json(_opts = nil)
      MultiJson.dump(@data)
    end
  end
end
