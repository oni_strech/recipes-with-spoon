# frozen_string_literal: true

require 'moneta'
require 'dalli'

module Cache
  DEFAULT_TTL = 0
  STORE = Moneta.build do
    use :Lock
    use :Transformer, value: :marshal
    adapter :MemcachedDalli, backend: Dalli::Client.new(nil)
  end
  MUTEX = Moneta::Mutex.new(STORE, 'mutex')

  module_function

  def fetch(key, ttl: DEFAULT_TTL)
    value = STORE[key]
    return value if value

    MUTEX.synchronize do
      value = STORE[key]
      return value if value

      STORE.store(key, yield, expires: ttl)
    end
  end

  def write(key, payload, ttl: DEFAULT_TTL)
    STORE.store(key, payload, expires: ttl)
  end
end
