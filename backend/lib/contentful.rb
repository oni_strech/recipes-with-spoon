# frozen_string_literal: true

module Contentful
  ENVIRONMENT =
    ENV.fetch('CONTENTFUL_ENVIRONMENT') { raise 'please set env variable CONTENTFUL_ENVIRONMENT' }

  ACCESS_TOKEN =
    ENV.fetch('CONTENTFUL_ACCESS_TOKEN') { raise 'please set env variable CONTENTFUL_ACCESS_TOKEN' }

  SPACE_ID =
    ENV.fetch('CONTENTFUL_SPACE_ID') { raise 'please set env variable CONTENTFUL_SPACE_ID' }
end
