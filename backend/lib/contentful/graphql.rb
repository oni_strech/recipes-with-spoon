# frozen_string_literal: true

require 'typhoeus'

module Contentful
  class Graphql
    URL = "https://graphql.contentful.com/content/v1/spaces/#{SPACE_ID}/environments/#{ENVIRONMENT}"
    HEADERS = {
      'Content-Type' => 'application/json',
      'Authorization' => "Bearer #{ACCESS_TOKEN}"
    }.freeze

    def self.post(query)
      Typhoeus.post(URL, headers: HEADERS, body: MultiJson.dump(query))
    end
  end
end
