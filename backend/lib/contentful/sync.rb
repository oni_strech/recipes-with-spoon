# frozen_string_literal: true

require 'typhoeus'

module Contentful
  class Sync
    BASE_URL = "https://cdn.contentful.com/spaces/#{SPACE_ID}/sync"
    HEADERS = {'Authorization' => "Bearer #{ACCESS_TOKEN}"}.freeze
    ITEMS_LIMIT = 100
    DELAY_IN_SECONDS = 60

    def initialize(content_type, type:)
      @sync_url = BASE_URL
      @params = {
        type: type,
        content_type: content_type,
        initial: true,
        limit: ITEMS_LIMIT
      }
    end

    def run
      loop do
        response = Typhoeus.get(@sync_url, headers: HEADERS, params: @params)
        raise StandardError, response.body if response.failure?

        body = MultiJson.load(response.body)
        @params = nil
        @sync_url = body.fetch('nextSyncUrl')

        Array(body['items']).each { |item| yield(item) }

        sleep DELAY_IN_SECONDS
      end
    end
  end
end
