# frozen_string_literal: true

require 'oj'
require 'zeitwerk'
require 'multi_json'
require 'multi_json/adapters/oj'
require 'hanami/controller'

require_relative 'initializers/types'

loader = Zeitwerk::Loader.new
loader.push_dir(File.expand_path('../lib', __dir__))
loader.push_dir(File.expand_path('../app', __dir__))
loader.setup
