# frozen_string_literal: true

require 'hanami/router'
require 'rack/common_logger'
require 'rack/cors'

RACK_ENV = ENV.fetch('RACK_ENV', 'development')

routes = Hanami::Router.new(namespace: Controllers) do
  resources 'recipes', only: %i[index show]
end

Application = Rack::Builder.app do
  use Rack::CommonLogger
  unless RACK_ENV == 'production'
    use Rack::Cors do
      allow do
        origins '*'
        resource '*', headers: :any, methods: [:get]
      end
    end
  end

  run routes
end
