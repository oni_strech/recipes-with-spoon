# frozen_string_literal: true

threads_count = ENV.fetch('PUMA_THREADS', 16).to_i
threads threads_count, 2 * threads_count

port ENV.fetch('PORT') { 8080 }
