# frozen_string_literal: true

require_relative 'config/boot'
require_relative 'config/application'

run Application
